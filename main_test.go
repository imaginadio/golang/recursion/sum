package main

import (
	"testing"

	"github.com/go-playground/assert/v2"
)

var arr = []int{1, 2, 3, 4, 5}

const expected = 15

func TestSumRecursive(t *testing.T) {
	t.Run("OK", func(t *testing.T) {
		result := SumRecursive(arr)
		assert.Equal(t, expected, result)
	})
}

func BenchmarkSumRecursive(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SumRecursive(arr)
	}
}

func TestSumLinear(t *testing.T) {
	t.Run("OK", func(t *testing.T) {
		result := SumLinear(arr)
		assert.Equal(t, expected, result)
	})
}

func BenchmarkSumLinear(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SumLinear(arr)
	}
}
