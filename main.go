package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4, 5}
	fmt.Println(SumRecursive(arr))
	fmt.Println(SumLinear(arr))
}

func SumRecursive(arr []int) int {
	if len(arr) == 0 {
		return 0
	} else if len(arr) == 1 {
		return arr[0]
	} else {
		return arr[0] + SumRecursive(arr[1:])
	}
}

func SumLinear(arr []int) int {
	var result int
	for i := range arr {
		result += arr[i]
	}
	return result
}
